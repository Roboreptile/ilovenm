# IloveNM

Instructions:
1. Put images to the to_conv folder
2. Run the file
3. ....
4. Profit!

Uses python v3.x + PIL module (python -m pip install pillow)

Short desc: takes mages from conv folder, converts them to jpg grayscale, and resizes them so that image size is under 100kb and the dimensions of the image are the greatest possible.

