from PIL import Image
from io import BytesIO
import os

print("WARNING: Verify the files size afterwards. For some reason Windows likes to increase file size by around 10%")
name = "PiotrKrzeminski"
sheet = input("Sheet name: ")
file_nr = 1
for file in os.listdir("./to_convert"):
    if file.split(".")[-1] == "txt": continue
    img = Image.open("./to_convert/"+file)
    img = img.convert('L')
    w = img.width
    h = img.height
    scalar = 0.5
    while True:
        scaled_img = img.resize(size = (int(w*scalar),int(h*scalar)), resample=Image.LANCZOS)
        tmp = BytesIO()
        scaled_img.save(tmp,"jpeg")
        if tmp.tell()<95000:
            scaled_img.save("./output/"+name+sheet+str(file_nr)+'.jpg')
            print(file,"converted!")
            file_nr+=1
            break
        else: scalar -= 0.01